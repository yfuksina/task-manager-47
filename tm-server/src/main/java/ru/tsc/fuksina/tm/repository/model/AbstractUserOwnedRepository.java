package ru.tsc.fuksina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.fuksina.tm.comparator.CreatedComparator;
import ru.tsc.fuksina.tm.comparator.DateStartComparator;
import ru.tsc.fuksina.tm.comparator.StatusComparator;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, final @NotNull M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateStartComparator.INSTANCE) return "status";
        else return "name";
    }

}
